chicken-isaac
=============
Bindings to Bob Jenkins' [ISAAC CSPRNG][isaac].

[isaac]: http://burtleburtle.net/bob/rand/isaacafa.html

Requirements
------------
 - [module-declarations](http://api.call-cc.org/doc/module-declarations)

Installation
------------
    $ chicken-install isaac

Documentation
-------------
Documentation is available on [chickadee][] and the [CHICKEN wiki][wiki].

[wiki]: http://wiki.call-cc.org/egg/isaac
[chickadee]: http://api.call-cc.org/doc/isaac

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
Public domain.
