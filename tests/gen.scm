;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Generates output suitable for testing with `dieharder -g 202`.
;;;

(use isaac srfi-4 srfi-13)

(define type #\d)
(define count 500000000)
(define numbit 32)

(define isaac
  (make-isaac
   (with-input-from-file "/dev/urandom"
    (lambda () (u8vector->blob/shared (read-u8vector 2048))))))

(print "type: " type)
(print "count: " count)
(print "numbit: " numbit)
(do ((i 0 (add1 i)))
    ((= i count))
  (print (string-pad (number->string (isaac)) 10)))
